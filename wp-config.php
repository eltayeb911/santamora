<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'santamora_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


define( 'WP_MEMORY_LIMIT', '64M' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u,[/qe?5= c2;Vg6+2o~cA8JjV=D.LTdpT_s5`sa|~pxbPal/iA9;dLb27Q-jH;4');
define('SECURE_AUTH_KEY',  '+|XI0QFTIY,xP=BRU64Xpt#.Hd)<A#CG}fkpRI`Fa;E=u:V.=FY/D}^/4+y(gQ&`');
define('LOGGED_IN_KEY',    '}oN%nkOm8QwTy4dBg:JyU%,/WW@0>A~V)a+BpNR)4l@C:rj:oRdj!,s3~3[=U-4.');
define('NONCE_KEY',        'sGU]#thv67kB#i.>KL={KfGFm:~#^2OIu/o1j U2{=F)Uu~_=ndWz_N|QUZHXW-7');
define('AUTH_SALT',        'ecVZ$U}BGK(os n-$UP1O@OY4Q^H}CI|on?%dOx0Z;/;o<$;AHW_G;+hZ,.r!D6p');
define('SECURE_AUTH_SALT', '$#Oy*q4=XR+L2$>QQ(Mk.0q}wnQK&q9&_7UObf#*+N5]d0UP*CiM[O+~ZcY&`%b$');
define('LOGGED_IN_SALT',   '[K0,i1(^ZZy00$~MThKb4{WMg#7iGo7!Pky,%<x@(qD1?$g*uN/=>AcWq =Swd%h');
define('NONCE_SALT',       '6u6{R$~/9S8WdtycW^X!+Zl&If[CGR+`SlG`vk_]h5lj~w2k<@c8NL/`sjyI7na}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sant_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
